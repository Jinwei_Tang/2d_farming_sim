{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 7,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 4,
  "bbox_right": 12,
  "bbox_top": 14,
  "bbox_bottom": 23,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 17,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"73432b3e-7c2a-4c0b-9b1c-319dce44e920","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"73432b3e-7c2a-4c0b-9b1c-319dce44e920","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"73432b3e-7c2a-4c0b-9b1c-319dce44e920","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dc17bfe6-b265-4236-be5a-c7fd1b9db604","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dc17bfe6-b265-4236-be5a-c7fd1b9db604","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"dc17bfe6-b265-4236-be5a-c7fd1b9db604","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e496c782-835e-4587-9995-a883c3d8357e","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e496c782-835e-4587-9995-a883c3d8357e","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"e496c782-835e-4587-9995-a883c3d8357e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f137b8d1-5c2a-42d9-a98a-84c26b3132d4","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f137b8d1-5c2a-42d9-a98a-84c26b3132d4","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"f137b8d1-5c2a-42d9-a98a-84c26b3132d4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"12bd1a0c-27ad-4d7f-833c-b2ce2b4966ba","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"12bd1a0c-27ad-4d7f-833c-b2ce2b4966ba","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"12bd1a0c-27ad-4d7f-833c-b2ce2b4966ba","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cfc79db5-5817-458e-8fea-999ebf9218f2","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cfc79db5-5817-458e-8fea-999ebf9218f2","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"cfc79db5-5817-458e-8fea-999ebf9218f2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7f61a942-ccd8-4bed-aa8a-7deacc359b6f","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7f61a942-ccd8-4bed-aa8a-7deacc359b6f","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"7f61a942-ccd8-4bed-aa8a-7deacc359b6f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6d085c63-9617-4ace-a03e-6f0e6f34b9c1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6d085c63-9617-4ace-a03e-6f0e6f34b9c1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"6d085c63-9617-4ace-a03e-6f0e6f34b9c1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ecd6b9f3-7dcd-4048-aa7f-29c4b42b304d","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ecd6b9f3-7dcd-4048-aa7f-29c4b42b304d","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"ecd6b9f3-7dcd-4048-aa7f-29c4b42b304d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"236ed5e0-d9da-47d4-bd1e-f8d118c72add","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"236ed5e0-d9da-47d4-bd1e-f8d118c72add","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"236ed5e0-d9da-47d4-bd1e-f8d118c72add","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4557f8b8-32d0-4f8a-aad2-a287c7f80feb","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4557f8b8-32d0-4f8a-aad2-a287c7f80feb","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"4557f8b8-32d0-4f8a-aad2-a287c7f80feb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"996eb345-2f9e-4019-b1c3-c59baa18ed09","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"996eb345-2f9e-4019-b1c3-c59baa18ed09","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"996eb345-2f9e-4019-b1c3-c59baa18ed09","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d96f0e59-7254-4d21-8f76-1a3df4cb8b5f","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d96f0e59-7254-4d21-8f76-1a3df4cb8b5f","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"d96f0e59-7254-4d21-8f76-1a3df4cb8b5f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"96dc5335-6172-4c28-aae6-5dd8ca3d8983","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"96dc5335-6172-4c28-aae6-5dd8ca3d8983","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"96dc5335-6172-4c28-aae6-5dd8ca3d8983","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d4569234-ebe1-4688-833b-2a8a6a649f44","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d4569234-ebe1-4688-833b-2a8a6a649f44","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"d4569234-ebe1-4688-833b-2a8a6a649f44","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4a035f53-9fb5-4a67-9205-664c6f2e1102","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4a035f53-9fb5-4a67-9205-664c6f2e1102","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"4a035f53-9fb5-4a67-9205-664c6f2e1102","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c2b59311-4c47-432a-adcb-7914a7cd8cf5","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c2b59311-4c47-432a-adcb-7914a7cd8cf5","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"c2b59311-4c47-432a-adcb-7914a7cd8cf5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"36a534f8-d139-4dff-b29b-aa79e5288ba0","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"36a534f8-d139-4dff-b29b-aa79e5288ba0","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"36a534f8-d139-4dff-b29b-aa79e5288ba0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"181409e4-7725-4dc2-8d0e-54f7bf20b3f2","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"181409e4-7725-4dc2-8d0e-54f7bf20b3f2","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"181409e4-7725-4dc2-8d0e-54f7bf20b3f2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fbc8bd76-3d5d-4b6a-821c-8876d1af9308","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fbc8bd76-3d5d-4b6a-821c-8876d1af9308","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"fbc8bd76-3d5d-4b6a-821c-8876d1af9308","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"db3adcb6-fb28-4ea4-96df-654cfa73c190","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"db3adcb6-fb28-4ea4-96df-654cfa73c190","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"db3adcb6-fb28-4ea4-96df-654cfa73c190","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"15b7924e-7352-4f5e-8e90-d4f7d8d51204","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"15b7924e-7352-4f5e-8e90-d4f7d8d51204","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"15b7924e-7352-4f5e-8e90-d4f7d8d51204","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"481b7820-eddc-445b-98c2-7464f4dffb3c","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"481b7820-eddc-445b-98c2-7464f4dffb3c","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"481b7820-eddc-445b-98c2-7464f4dffb3c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"783b3462-95e2-48fd-98be-f685d23279d6","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"783b3462-95e2-48fd-98be-f685d23279d6","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"783b3462-95e2-48fd-98be-f685d23279d6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4866cfe1-4bbf-4d5b-b2d2-a2c49593992d","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4866cfe1-4bbf-4d5b-b2d2-a2c49593992d","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"4866cfe1-4bbf-4d5b-b2d2-a2c49593992d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5fa2024b-30fa-4bb0-b7f3-8bac87de56c9","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5fa2024b-30fa-4bb0-b7f3-8bac87de56c9","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"5fa2024b-30fa-4bb0-b7f3-8bac87de56c9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ed795cef-483c-438a-85eb-12c8ba91e97f","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ed795cef-483c-438a-85eb-12c8ba91e97f","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"ed795cef-483c-438a-85eb-12c8ba91e97f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0f43c778-34ae-42b4-85db-39b4859d5e89","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0f43c778-34ae-42b4-85db-39b4859d5e89","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"0f43c778-34ae-42b4-85db-39b4859d5e89","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d2b9b3e1-c648-479c-b2af-e7b4962e8947","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d2b9b3e1-c648-479c-b2af-e7b4962e8947","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"d2b9b3e1-c648-479c-b2af-e7b4962e8947","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2b6e94b1-55ac-434d-8ecc-f82bb9ec5fb4","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2b6e94b1-55ac-434d-8ecc-f82bb9ec5fb4","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"2b6e94b1-55ac-434d-8ecc-f82bb9ec5fb4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c93d3cb3-406a-4c1f-9b56-aace4f9a7553","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c93d3cb3-406a-4c1f-9b56-aace4f9a7553","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"c93d3cb3-406a-4c1f-9b56-aace4f9a7553","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d9c038f0-7fc1-456d-ac78-26103d6e3628","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d9c038f0-7fc1-456d-ac78-26103d6e3628","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"LayerId":{"name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","name":"d9c038f0-7fc1-456d-ac78-26103d6e3628","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 32.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5e711a45-7b66-4d1c-841a-c340babd569f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"73432b3e-7c2a-4c0b-9b1c-319dce44e920","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0ba6d468-8849-4342-9418-098d7295103c","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dc17bfe6-b265-4236-be5a-c7fd1b9db604","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"63a1fc01-16ce-4943-8a80-196c63a35634","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e496c782-835e-4587-9995-a883c3d8357e","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"65962ae3-c80d-44c7-9844-89a703ad30a8","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f137b8d1-5c2a-42d9-a98a-84c26b3132d4","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"914dca26-80f4-4a0c-b394-0b698f81c002","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"12bd1a0c-27ad-4d7f-833c-b2ce2b4966ba","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a9d19918-7514-4274-bb34-8062bb383806","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cfc79db5-5817-458e-8fea-999ebf9218f2","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2f440833-b894-4199-860f-5b1a1613c671","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7f61a942-ccd8-4bed-aa8a-7deacc359b6f","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b3867ca8-834d-4e5f-886a-dbad5cef3590","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6d085c63-9617-4ace-a03e-6f0e6f34b9c1","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4493faa8-f7d1-48d3-b0ab-e3362646a34c","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ecd6b9f3-7dcd-4048-aa7f-29c4b42b304d","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"133b92eb-b91b-443b-922e-3e3ca162e054","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"236ed5e0-d9da-47d4-bd1e-f8d118c72add","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f9f67416-e59d-4658-8c03-c3d22ecf539b","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4557f8b8-32d0-4f8a-aad2-a287c7f80feb","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"25d144ae-09ca-480f-bb4e-2ada5ae6af91","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"996eb345-2f9e-4019-b1c3-c59baa18ed09","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a14dbc66-62d6-42c0-8f2c-adeb0bee3a04","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d96f0e59-7254-4d21-8f76-1a3df4cb8b5f","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5e0f6382-2be1-4e92-8028-1a1b5d356630","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"96dc5335-6172-4c28-aae6-5dd8ca3d8983","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"99ebf256-a140-4165-9b6a-f2032879768a","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d4569234-ebe1-4688-833b-2a8a6a649f44","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7c88fd78-5277-496b-ad52-685b08bb872c","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4a035f53-9fb5-4a67-9205-664c6f2e1102","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"35e1070e-2a56-4577-9be3-de2692a9812d","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c2b59311-4c47-432a-adcb-7914a7cd8cf5","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5953d381-a5c5-48c4-bf71-0d3e8110a455","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"36a534f8-d139-4dff-b29b-aa79e5288ba0","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"19e77577-4b15-46ef-a7aa-0dc8bc764ba3","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"181409e4-7725-4dc2-8d0e-54f7bf20b3f2","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"faf5e4bf-4a3e-45e9-86b5-65d8cdaa210e","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fbc8bd76-3d5d-4b6a-821c-8876d1af9308","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4cef09f2-7a61-465e-87af-99e316d3e307","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"db3adcb6-fb28-4ea4-96df-654cfa73c190","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c4975f0d-234f-400e-8010-b8e9afd5cb8e","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"15b7924e-7352-4f5e-8e90-d4f7d8d51204","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1395ac9e-debd-46af-af56-96922a2f9c84","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"481b7820-eddc-445b-98c2-7464f4dffb3c","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"aebebe77-d11d-4909-9492-faaf48277206","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"783b3462-95e2-48fd-98be-f685d23279d6","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"64dbefa6-ace3-4df9-a032-a3efc930cb18","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4866cfe1-4bbf-4d5b-b2d2-a2c49593992d","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"07b58b12-8da4-409d-a247-16a2897595f4","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5fa2024b-30fa-4bb0-b7f3-8bac87de56c9","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d5d94251-7060-44db-ac4f-536c96ec1b46","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ed795cef-483c-438a-85eb-12c8ba91e97f","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9b5f6449-0268-4adf-a69d-dd1266e2eb11","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0f43c778-34ae-42b4-85db-39b4859d5e89","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"91b49a6e-b87c-471d-8db1-881e9edd8bf9","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d2b9b3e1-c648-479c-b2af-e7b4962e8947","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6bd281db-a64c-4f27-bec4-505ed7dab1f1","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2b6e94b1-55ac-434d-8ecc-f82bb9ec5fb4","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d1c5fc5a-b450-4907-a335-70152571e782","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c93d3cb3-406a-4c1f-9b56-aace4f9a7553","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a913a3e0-43f8-4227-965d-f61379e5deb5","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d9c038f0-7fc1-456d-ac78-26103d6e3628","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 8,
    "yorigin": 24,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_userCat_run","path":"sprites/spr_userCat_run/spr_userCat_run.yy",},
    "resourceVersion": "1.3",
    "name": "spr_userCat_run",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"0753c1a5-fa42-45c6-b18f-0603a604e0f1","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "User",
    "path": "folders/Sprites/User.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_userCat_run",
  "tags": [],
  "resourceType": "GMSprite",
}