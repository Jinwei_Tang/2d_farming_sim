// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ScreenShake(input1, input2){
	with (global.iCamera){
		if (input1>shakeRemain){
			shakeMagnitude = input1;
			shakeRemain = shakeMagnitude;
			shakeLength = input2;
		}
	}
}