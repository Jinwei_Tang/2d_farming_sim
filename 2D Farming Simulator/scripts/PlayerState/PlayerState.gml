// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerStateRoll(){
	//set direction and speed of the roll
	hsp = lengthdir_x(speedRoll, direction);
	vsp = lengthdir_y(speedRoll, direction);
	
	moveDistanceRemaining = max(0,moveDistanceRemaining-speedRoll);
	var _collided = PlayerCollision();
	
	sprite_index = spriteRoll;
	var _totalFrames = sprite_get_number(sprite_index)/4
	image_index = (CARDINAL_DIR*_totalFrames) + min(((1-(moveDistanceRemaining/distanceRoll)) * _totalFrames), _totalFrames -1);
	
	//Change State
	if (moveDistanceRemaining <=0)
	{
		state = PlayerStateFree;
	}
	
	if(_collided){
		state = PlayerStateBonk;
		moveDistanceRemaining = distanceBonk;
		ScreenShake(4, 30)
	}
}

function PlayerStateFree(){
	//set magnitude of the direction
	hsp = lengthdir_x(inputMagnitude*walkspeed,inputDirection)
	vsp = lengthdir_y(inputMagnitude*walkspeed,inputDirection)

	//Collision and movement
	PlayerCollision();

	// update sprite_index
	var _oldSprite = sprite_index;
	if (inputMagnitude != 0){
		direction = inputDirection
		sprite_index = spriteRun
	}else{
		sprite_index = spriteIdel
	}
	if(_oldSprite != sprite_index) localFrame = 0

	//Update Image Index
	PlayerAnimateSprite();
	if (keyAttack){
		state = PlayerStateAttack;
		stateAttack = attackSlash;
	}
	
	if (keyInteract){
		var _activateX = lengthdir_x(10, direction);
		var _activateY = lengthdir_y(10, direction);
		activate = instance_position(x+_activateX, y+_activateY,prt_entity);
		if(activate !=noone and activate.entityActivateScript !=-1){
		ScriptExecuteArray(activate.entityActivateScript, activate.entityActivateArgs);
			
			if (activate.entityNPC)
			{
				with (activate)
				{
					direction = point_direction(x,y,other.x,other.y);
					image_index = CARDINAL_DIR;
				}
			
			}
		
		}
	}
	//activate key logic
	if (keyActivate){
			state = PlayerStateRoll;
			moveDistanceRemaining = distanceRoll;
	}
	
}

function PlayerStateLocked(){
	
}

function PlayerStateBonk(){
	//set direction and speed of the roll
	hsp = lengthdir_x(speedBonk, direction-180);
	vsp = lengthdir_y(speedBonk, direction-180);
	
	moveDistanceRemaining = max(0,moveDistanceRemaining-speedBonk);
	var _collided = PlayerCollision();
	
	sprite_index = spr_userCat_hurt;
	if (CARDINAL_DIR >2) image_index = CARDINAL_DIR-2;
	else image_index = CARDINAL_DIR +2;
	
	
	//chaneg height
	
z = sin(((moveDistanceRemaining/distanceBonk)*pi))*distanceBonkHeight;
	
	
	//Change State
	if (moveDistanceRemaining <=0)
	{
		state = PlayerStateFree;
	}
	
}

function PlayerStateAttack(){
	script_execute(stateAttack)

}

function attackSlash(){
	if(sprite_index != spr_userCat_attack){
		sprite_index = spr_userCat_attack;
		localFrame = 0;
		image_index = 0;
		
		if (!ds_exists(hitByAttack,ds_type_list))hitByAttack = ds_list_create();
		ds_list_clear(hitByAttack);
	}
	CalcAttack(spr_userCat_attackHB);
	
	PlayerAnimateSprite();
	if (animationEnd)
	{
		state = PlayerStateFree;
		animationEnd = false;
	}
}


function playerStateTransition(){
	PlayerCollision();
	PlayerAnimateSprite();
}
