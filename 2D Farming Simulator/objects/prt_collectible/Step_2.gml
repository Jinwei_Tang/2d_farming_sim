flash = max(0, flash-0.05);
fric = 0.05;
if(z==0) fric = 0.10;

if (instance_exists(obj_user))
{
	var _px = obj_user.x;
	var _py = obj_user.y;
	var _dist = point_distance(x,y,_px,_py);
	if(_dist < 16)
	{
		spd +=0.25;
		direction = point_direction(x,y,_px,_py);
		spd = min(spd,3);
		fric = 0;
	
	}
}



x+=lengthdir_x(spd,direction);
y+=lengthdir_x(spd,direction);
spd = max(spd-fric,0);
depth = -bbox_bottom;