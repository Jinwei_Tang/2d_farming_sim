//initialize Globals

randomize();

//gloabl values to get access
global.gamePaused = false;
global.iCamera = instance_create_layer(0,0,layer,obj_camera);
global.textSpeed = 0.75;
global.targetRoom = -1;
global.targetX = -1;
global.targetY = -1;
global.targetDirection = 0;
global.inventory = ds_map_create();
global.inventoryItems = ds_list_create();
global.showInventory = true;

surface_resize(application_surface, RESOLUTION_W,RESOLUTION_H);
room_goto(ROOM_START);