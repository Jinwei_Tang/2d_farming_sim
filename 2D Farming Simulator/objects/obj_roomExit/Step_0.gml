if (instance_exists(obj_user)) && (position_meeting(obj_user.x,obj_user.y,id))
{
	global.targetRoom = targetRoom;
	global.targetX = targetX;
	global.targetY = targetY;
	global.targetDirection = obj_user.direction;
	with (obj_user) state = playerStateTransition;
	RoomTransition(TRANS_TYPE.SLIDE,targetRoom);
	instance_destroy();
}