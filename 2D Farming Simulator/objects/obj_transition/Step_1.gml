/// @description Insert description here
// You can write your code in this editor
with (obj_user) state = playerStateTransition;

if (leading == OUT)
{
	percent = min(1,percent + TRANSITION_SPEED);
	if (percent>=1)
	{
		
		room_goto(target);
		
		leading = IN;
	}
}
else
{
	percent = max(0,percent - TRANSITION_SPEED);
	if (percent <=0)
	{
		with(obj_user) state = PlayerStateFree;
		instance_destroy();
	}
}