draw_sprite(spr_shadow,0,x,y);

draw_sprite_ext(
	sprite_index,
	image_index,
	floor(x),
	floor(y-z),
	image_xscale,
	image_yscale,
	image_angle,
	image_blend,
	image_alpha
)

if(global.showInventory){
	
	var len = ds_list_size(global.inventoryItems);
	
	for (var i = 0; i<len; i++){
		
		var key = ds_list_find_value(global.inventoryItems,i);
		var value = ds_map_find_value(global.inventory,key);
		
		var cam = camera_get_active();
		var camX = camera_get_view_x(cam);
		var camY = camera_get_view_y(cam);
		var spr = object_get_sprite(key)
		
		draw_sprite(spr,0,camX+GRID/2,camY+(i*GRID)+GRID/2);
		draw_text_transformed(camX + GRID , camY+(i*GRID), ": "+string(value),0.5,0.5,0)
	}
	
}