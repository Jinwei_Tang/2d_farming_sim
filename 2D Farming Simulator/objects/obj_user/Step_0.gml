//Player input
key_left = keyboard_check(ord("A"));
key_right = keyboard_check(ord("D"));
key_up = keyboard_check(ord("W"));
key_down = keyboard_check(ord("S"));
keyActivate = keyboard_check_pressed(vk_space)
keyInteract = keyboard_check_pressed(ord("E"));
keyAttack = keyboard_check_pressed(vk_shift)||mouse_check_button_pressed(mb_left)
keyInventory = keyboard_check_pressed(vk_tab)
if(keyInventory)global.showInventory = !global.showInventory;


//Movement calculation

inputDirection = point_direction(0,0,key_right-key_left,key_down-key_up)
inputMagnitude = (key_right-key_left !=0)||(key_down-key_up !=0)

if (!global.gamePaused) script_execute(state);

depth = -bbox_bottom;